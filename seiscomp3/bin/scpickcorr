#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Seiscomp3 module which correct Pick time from a value stored in a CSV
file.

:copyright:
    EOST (École et Observatoire des Sciences de la Terre)
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""
from collections import defaultdict
import csv
from datetime import datetime
import os
from random import randint
import sys
import traceback

from seiscomp3 import Client, Config, Core, DataModel, Logging, Seismology


def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


class LocatorException(Exception):
    """Raised when the relocation can't be performed"""


class UnknownLocatorException(Exception):
    """Raised when the locator isn't available"""


class PickCorrector(Client.Application):
    """
    Correct Pick time from a CSV data file.
    """

    # Convert an evaluation mode string to the correct enum
    ENUM_EVALUATION_MODES = {
        'manual': DataModel.MANUAL,
        'automatic': DataModel.AUTOMATIC,
    }

    # Convert an evaluation mode enum to the correct string
    STR_EVALUATION_MODES = {
        DataModel.MANUAL: 'manual',
        DataModel.AUTOMATIC: 'automatic',
    }

    def __init__(self, argc, argv):
        Client.Application.__init__(self, argc, argv)
        self.setMessagingEnabled(True)
        self.setDatabaseEnabled(True, True)
        self.setDaemonEnabled(False)
        self.setMessagingUsername('')

        # Store the pick time corrections in a double dict
        # Example: self.pick_fixes['origin_id']['pick_id'] = time_correction
        self.pick_fixes = defaultdict(dict)
        # Store the origin parents (an event) in a dict
        self.origin_parents = {}

        # Store locators used
        self.locators = {}

        # Mode configuration
        self.mode_test = False
        # Locator configuration
        self.fixed_depth = None
        self.distance_cut_off = None
        self.ignore_initial_location = False
        self.use_origin_locator = False
        # Input configuration
        self.csv_filename = None
        # Output configuration
        self.output_author = os.path.basename(__file__)
        self.output_agency_id = None
        self.output_evaluation_mode = DataModel.AUTOMATIC

    def init(self):
        try:
            if not Client.Application.init(self):
                return False

            self.db_query = DataModel.DatabaseQuery(self.database())

            # Configure default locator to be used
            try:
                self._init_locator(self.locator_type)
            except UnknownLocatorException:
                Logging.error('Locator %s not available -> abort'
                              % self.locator_type)
                return False

            # Read CSV file
            self._read_file(self.csv_filename)

            return True
        except:
            info = traceback.format_exception(*sys.exc_info())
            for i in info:
                sys.stderr.write(i)
            raise

        return False

    def initConfiguration(self):
        if not Client.Application.initConfiguration(self):
            return False

        # Mode configuration
        try:
            self.mode_test = self.configGetBool('mode.test')
        except (Config.Exception, RuntimeError):
            pass

        # Locator configuration
        try:
            self.locator_type = \
                self.configGetString('locator.type')
        except (Config.Exception, RuntimeError):
            pass
        try:
            self.locator_profile = \
                self.configGetString('locator.profile')
        except (Config.Exception, RuntimeError):
            pass
        try:
            self.fixed_depth = self.configGetDouble('locator.fixedDepth')
        except (Config.Exception, RuntimeError):
            pass
        try:
            self.distance_cut_off = \
                self.configGetDouble('locator.distanceCutOff')
        except (Config.Exception, RuntimeError):
            pass
        try:
            self.ignore_initial_location = \
                self.configGetBool('locator.ignoreInitialLocation')
        except (Config.Exception, RuntimeError):
            pass
        try:
            self.use_origin_locator = \
                self.configGetBool('locator.useOriginLocator')
        except (Config.Exception, RuntimeError):
            pass

        # Output configuration
        try:
            self.output_author = self.configGetString('output.author')
        except (Config.Exception, RuntimeError):
            pass
        try:
            self.output_agency_id = self.configGetString('output.agencyID')
        except (Config.Exception, RuntimeError):
            pass
        try:
            str_mode = self.configGetString('output.evaluationMode')
            self.output_evaluation_mode = \
                PickCorrector.ENUM_EVALUATION_MODES[str_mode.lower()]
        except (Config.Exception, KeyError, RuntimeError):
            pass

        return True

    def createCommandLineDescription(self):
        """
        Define parameters available at the command execution
        """
        self.commandline().addGroup('Mode')
        self.commandline().addOption(
            'Mode',
            'test,T',
            'test mode, do not write object in database')

        self.commandline().addGroup('Locator')
        self.commandline().addStringOption(
            'Locator',
            'locator-type',
            'locator to use')
        self.commandline().addStringOption(
            'Locator',
            'locator-profile',
            'locator profile')
        self.commandline().addStringOption(
            'Locator',
            'fixed-depth',
            'fix depth')
        self.commandline().addStringOption(
            'Locator',
            'distance-cut-off',
            'distance cut off')
        self.commandline().addOption(
            'Locator',
            'ignore-initial-location',
            'ignore initial location')
        self.commandline().addOption(
            'Locator',
            'use-origin-locator',
            'use the primary origin locator if possible')

        self.commandline().addGroup('Input')
        self.commandline().addStringOption(
            'Input',
            'csv-filename',
            'CSV filename')

        self.commandline().addGroup('Output')
        self.commandline().addStringOption(
            'Output',
            'author,Au',
            'author of the new origin')
        self.commandline().addStringOption(
            'Output',
            'agencyID',
            'agency ID of the new origin')
        self.commandline().addStringOption(
            'Output',
            'evaluation-mode',
            'origin evaluation mode (manual or automatic)')

    def validateParameters(self):
        """
        Retrieve parameters passed at the command execution.
        """
        if Client.Application.validateParameters(self) is False:
            return False

        # Mode configuration
        self.mode_test = self.commandline().hasOption('test')

        # Locator configuration
        if self.commandline().hasOption('locator-type'):
            self.locator_type = \
                self.commandline().optionString('locator-type')
        if self.commandline().hasOption('locator-profile'):
            self.locator_profile = \
                self.commandline().optionString('locator-profile')
        if self.commandline().hasOption('fixed-depth'):
            fixed_depth = self.commandline().optionString('fixed-depth')
            self.fixed_depth = float(fixed_depth)
        if self.commandline().hasOption('distance-cut-off'):
            dcu = self.commandline().optionString('distance_cut_off')
            self.distance_cut_off = float(dcu)
        if self.commandline().hasOption('ignore-initial-location'):
            self.ignore_initial_location = True
        if self.commandline().hasOption('use-origin-locator'):
            self.use_origin_locator = True

        # Input configuration
        if self.commandline().hasOption('csv-filename'):
            self.csv_filename = \
                self.commandline().optionString('csv-filename')
        else:
            Logging.error('No CSV file given')
            return False

        # Output configuration
        if self.commandline().hasOption('author'):
            self.output_author = self.commandline().optionString('author')
        if self.commandline().hasOption('agencyID'):
            self.output_agency_id = \
                self.commandline().optionString('agencyID')
        if self.commandline().hasOption('evaluation-mode'):
            try:
                str_mode = \
                    self.commandline().optionString('evaluation-mode')
                self.output_evaluation_mode = \
                    PickCorrector.ENUM_EVALUATION_MODES[str_mode.lower()]
            except (KeyError, RuntimeError):
                Logging.error('Wrong evaluation-mode parameter')

        return True

    def _init_locator(self, locator_type):
        """
        Create a locator with the wanted locator type if it doesn't exist.

        If the locator isn't available, raise UnknownLocatorException.
        """
        try:
            return self.locators[locator_type.lower()]
        except KeyError:
            locator = Seismology.LocatorInterface.Create(locator_type)
            if not locator:
                raise UnknownLocatorException()

            locator.init(self.configuration())

            # Set distance cut off
            if self.distance_cut_off is not None:
                locator.setDistanceCutOff(self.distance_cut_off)

            # Set ignore initial location
            locator.setIgnoreInitialLocation(self.ignore_initial_location)

            self.locators[self.locator_type.lower()] = locator

            return locator

    def _read_file(self, filename):
        """
        Read the pick time corrections from a CSV file.
        """
        with open(filename) as f:
            reader = csv.reader(f, dialect="excel-tab")
            # Skip header
            next(reader, None)

            for line in reader:
                event_id = line[0].split('.')[0]
                origin_id = line[1]
                pick_id = line[2]
                time_correction = float(line[8])

                if not isclose(time_correction, 0):
                    self.pick_fixes[origin_id][pick_id] = time_correction
                    self.origin_parents[origin_id] = event_id

    def _generate_public_id(self, prefix):
        """
        Generate a publicID based on the current UTC datetime.
        """
        now = datetime.utcnow()
        random_number = randint(100000, 999999)
        return '{0}#{1:%Y%m%d%H%M%S.%f}.{2}_fix'.format(prefix, now,
                                                        random_number)

    def _load_picks(self, origin):
        """
        Load all picks associated to an origin (useful for the
        relocation).
        """
        picks = []
        for i in xrange(origin.arrivalCount()):
            arrival = origin.arrival(i)
            pick_id = arrival.pickID()
            pick = self.db_query.loadObject(DataModel.Pick.TypeInfo(), pick_id)
            pick = DataModel.Pick.Cast(pick)
            picks.append(pick)
        return picks

    def _load_inventory(self):
        """
        Load the inventory (useful for the relocation).
        """
        inv = DataModel.Inventory()
        self.db_query.loadNetworks(inv)
        for i in xrange(inv.networkCount()):
            self.db_query.load(inv.network(i))
        return inv

    def _get_locator_parameters(self, origin):
        """
        Get the locators with their parameters which will be used for
        the relocation.

        If the first locator can't relocate the origin, the second
        locator is used and so on.
        """
        locator = None
        parameters = []
        if self.use_origin_locator:
            try:
                locator = self._init_locator(origin.methodID().lower())
            except UnknownLocatorException:
                Logging.info('Locator %s not available' % origin.methodID())
            else:
                if self.fixed_depth is None:
                    # Locator with released depth
                    parameters.append((
                        locator,
                        origin.earthModelID(),
                        None
                    ))
                    # Locator with fixed depth
                    parameters.append((
                        locator,
                        origin.earthModelID(),
                        origin.depth().value()
                    ))
                else:
                    parameters.append((
                        locator,
                        origin.earthModelID(),
                        self.fixed_depth
                    ))
        else:
            # Try the default locator
            default_locator = self.locators[self.locator_type.lower()]
            if (default_locator is not locator or
                    self.locator_profile != origin.earthModelID()):
                if self.fixed_depth is None:
                    # Locator with released depth
                    parameters.append((
                        default_locator,
                        self.locator_profile,
                        None
                    ))
                    # Locator with fixed depth
                    try:
                        parameters.append((
                            default_locator,
                            self.locator_profile,
                            origin.depth().value()
                        ))
                    except (Core.ValueException, RuntimeError):
                        # Depth is not set
                        pass
                else:
                    parameters.append((
                        default_locator,
                        self.locator_profile,
                        self.fixed_depth
                    ))

        return parameters

    def _relocate(self, origin):
        """
        Relocate the origin passed in parameter and return the new
        origin.
        """
        # Load picks and inventory for the relocation. The variables
        # aren't used but they still are useful (even if they aren't
        # passed in parameters).
        picks = self._load_picks(origin)  # NOQA
        inventory = self._load_inventory()  # NOQA

        # Prepare a list of parameters to try different locator
        # parameters. This is useful when the relocation fails.
        parameters = self._get_locator_parameters(origin)

        # Let's go! Time to relocate!
        new_origin = None
        for locator, locator_profile, depth in parameters:
            locator.setProfile(locator_profile)
            if depth is None:
                Logging.info(("Try to relocate with locator %s, profile %s "
                             "and released depth")
                             % (locator.name(), locator_profile))
                locator.releaseDepth()
            else:
                Logging.info(("Try to relocate with locator %s, profile %s "
                             "and fixed depth %s")
                             % (locator.name(), locator_profile, depth))
                locator.setFixedDepth(depth)

            try:
                new_origin = locator.relocate(origin)
                break
            except (Core.GeneralException, RuntimeError) as e:
                # Several specific exception exists
                # (`Seismology.PickNotFoundException`,
                # `Seismology.LocatorException`,
                # `Seismology.StationNotFoundException`) but they
                # aren't used!!!
                Logging.error("Can't relocate")
                Logging.error(str(e))

        if new_origin is None:
            raise LocatorException()

        return new_origin

    def _set_origin_metadata(self, origin):
        """
        Set agencyID, authod, evaluation mode and status to the origin.
        """
        # There are some modifications to do on the generated origin
        if self.output_agency_id is not None:
            origin.creationInfo().setAgencyID(self.output_agency_id)
        origin.creationInfo().setAuthor(self.output_author)

        origin.setEvaluationMode(self.output_evaluation_mode)
        origin.setEvaluationStatus(DataModel.PRELIMINARY)

    def _update_preferred_origin(self, event):
        """
        Create a Journaling notifier to update the preferred origin of
        the event.
        """
        journaling = DataModel.Journaling()
        journal_entry = DataModel.JournalEntry()
        journal_entry.setCreated(Core.Time_GMT())
        journal_entry.setSender(self.output_author)
        # No doc at all for 'EvPrefOrgAutomatic' (see
        # seiscomp3/src/trunk/apps/processing/scevent/eventtool.cpp)
        journal_entry.setAction('EvPrefOrgAutomatic')
        journal_entry.setObjectID(event.publicID())
        journaling.add(journal_entry)

    def _send_pick(self, pick):
        """
        Add the Pick to the EventParameters.

        The database will be updated when the Origin will be sent.
        """
        if self.mode_test:
            return

        ep = DataModel.EventParameters()

        DataModel.Notifier.Enable()
        ep.add(pick)
        DataModel.Notifier.Disable()

    def _send(self, event, origin):
        """
        Add the origin to the event and update the database with the
        new objects.
        """
        if self.mode_test:
            return

        ep = DataModel.EventParameters()

        DataModel.Notifier.Enable()

        ep.add(origin)
        origin_reference = DataModel.OriginReference(origin.publicID())
        event.add(origin_reference)

        self._update_preferred_origin(event)

        DataModel.Notifier.Disable()

        message = DataModel.Notifier.GetMessage(True)
        self.connection().send(message)

    def _process_origin(self, event, origin, pick_fixes):
        """
        Correct Pick time for a given Origin.
        """
        for pick_id, time_correction in pick_fixes.items():
            Logging.info('Process pick %s' % pick_id)
            pick = self.db_query.loadObject(DataModel.Pick.TypeInfo(),
                                            pick_id)
            pick = DataModel.Pick.Cast(pick)

            if not pick:
                Logging.error("Can't find pick with publicID %s" % pick_id)
                continue

            # Create new Pick ID
            new_pick_id = self._generate_public_id('Pick')
            pick.setPublicID(new_pick_id)

            # Apply pick time correction
            pick_time = pick.time().value()
            new_pick_time = pick_time + Core.TimeSpan(time_correction)
            pick.time().setValue(new_pick_time)

            # Create new arrival
            for i in xrange(origin.arrivalCount()):
                arrival = origin.arrival(i)

                if pick_id == arrival.pickID():
                    arrival.setPickID(new_pick_id)
                    break

            self._send_pick(pick)

        # Relocate the origin
        try:
            new_origin = self._relocate(origin)
        except LocatorException:
            Logging.error("Can't relocate origin %s, new picks will be "
                          "added without relocation" % origin.publicID())
            new_origin_id = self._generate_public_id('Origin')
            origin.setPublicID(new_origin_id)
            self._set_origin_metadata(origin)
            self._send(event, origin)
        else:
            self._set_origin_metadata(new_origin)
            self._send(event, new_origin)

    def _process(self):
        """
        Correct Pick time for all origins of the CSV file.
        """
        for origin_id, pick_fixes in self.pick_fixes.items():
            origin = self.db_query.loadObject(DataModel.Origin.TypeInfo(),
                                              origin_id)
            origin = DataModel.Origin.Cast(origin)

            if not origin:
                Logging.error("Can't find origin with publicID %s" % origin_id)
                continue

            event_id = self.origin_parents[origin_id]
            event = self.db_query.getEventByPublicID(event_id)

            if not event:
                Logging.error("Can't find event with publicID %s for the "
                              "origin %s" % (event_id, origin_id))
                continue

            Logging.info('Process origin %s of event %s'
                         % (origin_id, event_id))

            self._process_origin(event, origin, pick_fixes)

    def run(self):
        try:
            self._process()

            return True
        except:
            info = traceback.format_exception(*sys.exc_info())
            for i in info:
                sys.stderr.write(i)
            raise

        return False


if __name__ == "__main__":
    app = PickCorrector(len(sys.argv), sys.argv)
    app()
