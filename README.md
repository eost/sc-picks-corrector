# Pick corrector!

This seiscomp3 module aims to correct pick time from a value stored in a CSV
file. A new origin is created with the correct picks and relocated.

## Install

Synchronise the source into seiscomp3 folder:

```bash
rsync -av seiscomp3/ /path/to/seiscomp3/
```

## Usage

First, regroup all picks in the same file. We can use something like that:

```bash
tail -n +2 file.txt >> data.csv
```

The `data.csv` looks like:

```csv
#IDevt	IDpori	IDpick	Net	Sta	Loc	Cha	Phase	Dt[s]_(Tnew=Tori+Dt)	Dtpicked[s]
geobs2013hidupzgo.xml	Origin#20170717074144.490535.41957	Pick#20170717074108.69541.41886	RT	BETS	00	ELZ	Pg	0.000000	0.015000
geobs2013hiemppnk.xml	Origin#20170717074559.141804.42174	Pick#20170717074557.131752.42162	RT	BETS	00	ELZ	Pg	0.000000	0.010000
geobs2013himrqoea.xml	Origin#20170925081730.833586.18378	Pick#20170717075027.897579.42567	RT	BETS	00	ELZ	Pg	0.000000	0.010000
```

Then we can use the pick corrector:

```bash
seiscomp exec scpickcorr --debug --csv-filename data.csv
```

> **Info**
>
> Some parameters can be set like the agency ID or the locator profile. You can
> either use `seiscomp exec scconfig` or the command line arguments to
> customize them. To list all the optional parameters, use the `--help` option:
>
> `seiscomp exec scpickcorr --help`
